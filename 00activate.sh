#! /bin/sh
### 00activate.sh ---

## Author: Prof. Dr. Jobst Hoffmann <j.hoffmann@fh-aachen.de>
## Version: $Id: 00activate.sh 0 <2023/02/20 17:32:51> ax006ho Exp $
##          update date and time by C-u M-x org-time-stamp
## Keywords:
## X-URL:

#=----------------------------------------------------------------------
#=- error handling according to
#=- https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail
#=- http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -Eeuo pipefail

trap "echo ERR trap fired!" ERR

#=----------------------------------------------------------------------
#=- global functions
#=-

##=---------------------------------------------------------------------
##=- <function name>:
##=- parameter/s:
##=-
##=- [<description>]

##=---------------------------------------------------------------------


#=----------------------------------------------------------------------
#=- start of executable code
main() {
    local rc=0
    usage="$0 {-a|-d} -- activate or deactivate\n\t\tthe development version"`
       `" of the listings package"
    cd ${HOME}/texmf/tex/latex/listings

    if [[ $# -eq 1 ]] && [[ "$1" == "-a" ]]; then
        # activate development versions
        if ls *.sty.devel 2> /dev/null; then
            for f in *.sty.devel; do
                mv ${f} ${f/.sty.devel/.sty}
            done
        fi
    elif [[ $# -eq 1 ]] && [[ "$1" == "-d" ]]; then
        # deactivate original version
        if ls *.sty 2> /dev/null; then
            for f in *.sty; do
                mv ${f} ${f/.sty/.sty.devel}
            done
        fi
    else
        printf "%b\n" "${usage}"
    fi

    cd -
    return ${rc}
} # end of function main


{
    ##=-----------------------------------------------------------------
    ##=- global variables

    ###=- load the BMS ...
    if ! . bms.sh; then
        exit 1
    fi
    ###=- ... and some modules
    if ! include "ctrl"; then   # "ctrl" provides several exit codes and
        exit 1                  # "log"
    fi

    main "${@}"
    exit "${?}"
} # end of executable code


# Local Variables:
# mode: sh
# outline-regexp: "^ *#+=-.*"
# End:

### 00activate.sh ends here
