# Line numbers that match the linerange specification #

<file:///home/ax006ho/TeX/texmf/source/latex/listings/issues/larsson/linerange_specification_00a.tex>
shows the standard behaviour of the linerange option: line numbering
happens to start at 1, lines omitted by the use of linerange aren't counted
at all.

The user Werner asked for a method to synchronize linerange and printed
line numbers.

The question put in [Line numbers that match the linerange
specification](https://tex.stackexchange.com/questions/110187/listings-line-numbers-that-match-the-linerange-specification
"linerange specifications") got several answers:

  * patch the \lst@GLI@ command by using the etoolbox package:
    <file:///home/ax006ho/TeX/texmf/source/latex/listings/issues/larsson/linerange_specification_00b.tex>
  * introduce a new option "matchrangestart":
    <file:///home/ax006ho/TeX/texmf/source/latex/listings/issues/larsson/linerange_specification_01.tex>
  * enhance the keyword numbers by the value leftliteral:
    <file:///home/ax006ho/TeX/texmf/source/latex/listings/issues/larsson/linerange_specification_02.tex>
  * redefine the macro \lst@MSkipToFirst:
    <file:///home/ax006ho/TeX/texmf/source/latex/listings/issues/larsson/linerange_specification_03.tex>

None of these solutions is perfect.

Here's a solution which comprises both of the aspects of the last example.

There are two new options:
  * consecutivenumbering (true, false) -> true

      this describes how ranges are numbered
      * consecutivenumbering=true stands for the standard numbering, that is
        the numbering of different ranges is consecutive. It can be used
        for documenting code:

            comment
            declaration
            comment
            code









  * rangegap (dimension) -> 0pt
