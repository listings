#! /bin/sh

[[ -L lstpatch.sty ]] && rm lstpatch.sty
ln -s lstpatch-v1.10c.sty lstpatch.sty

export PATH=/usr/local/texlive/2024/bin/x86_64-linux:.:/home/ax006ho/.local/bin:/usr/share/Modules/bin:/usr/lib64/openmpi/bin:/opt/local/packages/android-studio/bin:/usr/local/texlive/2024/bin/x86_64-linux:/opt/local/bin:/opt/gnu/bin:/usr/lib64/ccache:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin
export TEXINPUTS=.:/usr/local/texlive/2024/texmf-dist/tex/latex//:

pdflatex --jobname test_upquote_tl2024 test_upquote.tex

export PATH=/usr/local/texlive/2023/bin/x86_64-linux:.:/home/ax006ho/.local/bin:/usr/share/Modules/bin:/usr/lib64/openmpi/bin:/opt/local/packages/android-studio/bin:/usr/local/texlive/2024/bin/x86_64-linux:/opt/local/bin:/opt/gnu/bin:/usr/lib64/ccache:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin
export TEXINPUTS=.:/usr/local/texlive/2023/texmf-dist/tex/latex//:

[[ -L lstpatch.sty ]] && rm lstpatch.sty
ln -s lstpatch-v1.9.sty lstpatch.sty

pdflatex --jobname test_upquote_tl2023 test_upquote.tex
