# testcases in the caption directory #

  * The [first test provided by Markus
    Kohm](file:///home/ax006ho/texmf/source/latex/listings/testing/caption/book-listings-tocbasic-test-preview.tex)
    uses the
    [notlistings.sty](file:///home/ax006ho/texmf/source/latex/listings/contrib/kohm/notlistings.sty),
    it requires version v1.9 of the **listings** package, so the Makefile
    rule must be adapted accordingly. It uses the option
    `[enablepatch=all`, which isn't available for the release version

  * The [second test provided by Markus
    Kohm](file:///home/ax006ho/texmf/source/latex/listings/testing/caption/book-listings-tocbasic-test-release.tex)
    uses the
    [listings.sty](file:///home/ax006ho/texmf/source/latex/listings/listings.sty),
    it requires version at least v1.10 of the **listings** package, nothing
    needs to be done.

Both tests show the appearance of a list of listings. Markus Kohm added the
following text:

> Damit Du siehst, welchen Vorteil meine Version von listings auch für die
> Standardklassen mit sich bringt bzw. mit der nächsten Version von tocbasic
> bringen wird, anbei einmal ein Beispiel:
>
> - book-listings-tocbasic-test.tex (das ist die Testdatei)
> - tocbasic.sty (das ist eine Vorabversion von tocbasic v3.42)
> - notlistings.sty (das ist meine Version von listings allerdings mit alter
> Versionsnummer)
> - book-listings-tocbasic-test.pdf (das ist das Ergebnis)
>
> Schau dir vor allem das Inhaltsverzeichnis und das Listings-Verzeichnis an.
> Vergleiche das mit dem Ergebnis wahlweise mit deiner Version von listings oder
> mit einer alten Version von listings oder zwar mit neuer Version von listings
> aber ohne tocbasic.
>
> Natürlich darfst Du gerne auch andere tocbasic-Features, die mit \setuptoc
> gesetzt werden können, ausprobieren. Zum Vergleich kannst Du auch gerne einmal
> die Option beim Laden von tocbasic entfernen.
>
> Wahrscheinlich wird es bis zur Release noch die eine oder andere Änderung an
> tocbasic geben. Eventuell wird der Abstand im Verzeichnis zusätzlich
> konfigurierbar werden. Wird man sehen.


Then there are four cases for the caption option of the **listings**
package, which are based on two files:

    1. [caption_00.tex](file:///home/ax006ho/texmf/source/latex/listings/testing/caption/caption_00.tex)
    2. [caption_01.tex](file:///home/ax006ho/texmf/source/latex/listings/testing/caption/caption_01.tex)

The difference between the two files is the underlying documentclass (00:
book, 01: scrbook), the two files get formatted on the one hand using v1.8d of
the **listings** package, on the other hand with at least v1.10 of the
**listings** package, so that we have overall four test cases.
