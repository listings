#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
  /* declaring variables */
  int i;
  int limit;

  /* checking arguments */
  if ( argc > 1 ) {
    limit = atoi(argv[1]);
  } else {
    limit = 7;
  }

  /* counting lines */
  for (i = 1; i <= limit; i++) {
    printf("Line no. %3.0d\n", i);
  }

  return 0;
}
