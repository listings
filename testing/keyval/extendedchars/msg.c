#include <stdio.h>
#include <stdlib.h>

/*
Du musst nicht Vietnam oder China bemühen wenn du utf8 willst, dieser
Satz hier in einem Kommentar würde schon reichen um listings auf die
Nase fallen zu lassen, sprich ein copy paste von irdendwas mit Umlauten
oder anderen westlichen Akzenten produziert multi-byte UTF8

Da hast sicher recht, dass mit unicode engines das Problem dann nicht
mehr existiert da die alle UTF8 chars als tokens sehen, aber bei pdftex
ist das halt nicht der Fall und auf absehbare Zeit wird das der Standard
bleiben.

Als unsere Computer noch 8bit code pages benutzten war das auch kein
Problem aber mittlerweile ist es eins.
*/
int main(int argc, char** argv) {

  char* msg = "\
Man kann natürlich warten bis sich LuaTeX durchsetzt, was vermutlich \
irgendwann passieren wird, aber ich denk 5-10 Jahre gehen da noch ins Land\
";

  printf ("%s\n", msg);

}
