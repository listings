// von: http://www.linux-magazin.de/Ausgaben/2011/12/C-11

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>

void add3(int& i){
  i +=3;
}

int main(){
  std::cout << std::endl;

  std::vector<int> myVec1{1,2,3,4,5,6,7,8,9,10};
  std::cout << std::setw(20) << std::left << "myVec1: i->i+3:     ";
  std::for_each(myVec1.begin(),myVec1.end(),add3);
  for (auto v: myVec1) std::cout << std::setw(6) << std::left << v;

  std::cout << "\n\n";
}
