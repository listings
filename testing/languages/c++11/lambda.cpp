// von: http://www.linux-magazin.de/Ausgaben/2011/12/C-11

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

int main(){
  std::cout << std::endl;

  std::vector<int> myVec4{1,2,3,4,5,6,7,8,9,10};
  std::cout << std::setw(20) << std::left << "myVec4: i->3*i+5:   ";
  std::for_each(myVec4.begin(),myVec4.end(),[](int& i){i=3*i+5;});
  for (auto v: myVec4) std::cout << std::setw(6) << std::left << v;
  std::cout << "\n";

  std::vector<int> myVec5{1,2,3,4,5,6,7,8,9,10};
  std::cout << std::setw(20) << std::left << "myVec5: i->i*i   ";
  std::for_each(myVec5.begin(),myVec5.end(),[](int& i){i=i*i;});
  for (auto v: myVec5) std::cout << std::setw(6) << std::left << v;
  std::cout << "\n";

  std::vector<double> myVec6{1,2,3,4,5,6,7,8,9,10};
  std::for_each(myVec6.begin(),myVec6.end(),[](double& i){i=std::sqrt(i);});
  std::cout << std::setw(20) << std::left << "myVec6: i->sqrt(i): ";
  for (auto v: myVec6) std::cout << std::fixed << std::setprecision(2)
<< std::setw(6) << v ;

  std::cout << "\n\n";
}
