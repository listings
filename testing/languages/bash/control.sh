#! /bin/sh
### control.sh --- 

## Author: Prof. Dr. Jobst Hoffmann <j.hoffmann@fh-aachen.de>
## Version: $Id: control.sh,v 0.0 2014/08/12 18:26:35 ax006ho Exp $
## Keywords: 
## X-URL: 

number=12

for i in `seq 1 3`; do
    printf "this is number %d\n" $i
done

if [[ $number -lt 10 ]]; then
    echo "number is too small"
elif [[ $number -gt 11 ]]; then
    echo "number is big enough"
else
    echo "number is equal to 11"
fi

### control.sh ends here
