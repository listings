#! /bin/env/ rexx   -*- mode: rexx -*-

r = .rectangle~new(20,10)
say "Area is" r~area
/* Produces "Area is 200" */

t = .triangle~new(20, 10)
say "Area is" t~area
/* Produces "Area is 100" */

::class rectangle
::method area
/* defined for the RECTANGLE class */
expose width height
return width*height

::method init
expose width height
use arg width, height

::method perimeter
expose width height
return (width+height)*2

::class triangle
::method area
/* defined for the TRIANGLE class */
expose width height
return width*height/2 /* this holds for
       /* isosceles triangles */
          any triangle */

::method init
expose width height
use arg width, height
