#! /bin/env/ rexx

/* a little rexx script based on standard rexx */
say "Enter a first integer number"
pull a
say "Enter a second integer number"
pull b

say a b

call process_numbers
say result

filename = 'rexx_std.rexx'
call readfirstline
say 'result: ' || result

filename = 'rexx_std.rexx'
'head -n 1 '||filename
say 'rc: ' || rc

exit

process_numbers: procedure expose a b

sum = a + b
difference = a - b
product = a*b
quotient = a/b

result = "Sum: "||sum||", difference: "||difference||", product: "||,
  product||", quotient: "||quotient

return result

readfirstline: procedure expose filename

if lines(filename,'N') then do
   line = linein(filename)
end

return line
