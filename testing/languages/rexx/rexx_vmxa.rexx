#! /bin/env/ rexx  -*- mode: rexx -*-

/* a little Rexx script using Rexx VM/XA extensions */

/* Main program */
status = stream('rexx_vmxa.rexx') -- testing the state of this file
say status

numeric form engineering
say form()

a = 1.2
b = 123
q = a/b
say q

eps = calculate_eps()
say "machine epsilon: " eps

exit

calculate_eps: procedure
eps = 1.0
i = 0
do while 1 + eps > 1
    eps = eps/2
    i = i +  1
    say i": "1 + eps", "1
end
return eps*2
