%%% rexx_5.1-1.tex ---
%
%% Author: j.hoffmann@fh-aachen.de
%% Time-stamp: <2024-03-17 12:11:59 ax006ho>
%% Keywords:
%% X-URL:

%
% several simple test cases for Rexx (Classic, VM/XA, and ooRexx)
%
\documentclass[12pt,a4paper,english]{scrartcl}
                                % bei gemischtsprachigem Satz ist die
                                % zuletzt geladene Sprache aktiv
\IfClassLoadedTF{article}{%
  \usepackage{typearea}         % ein auf DIN A4 abgestimmtes Seitenformat,
}{}                             % wird nur bei article gebraucht

\usepackage{iftex}
\ifluatex                       % wenn mit dem neuen TeX-Prozessor
                                % gearbeitet wird
  %---- Eingabezeichensatz ---------------------------------------------------
                                % luatex unterstützt utf-8, also keine
                                % Festlegung des Eingabezeichensatzes
                                % erforderlich
  %---- Grundfont ------------------------------------------------------------
  \usepackage{fontspec}         % Festlegen der Fontverwaltung für LuaTeX.
  \defaultfontfeatures{Ligatures=TeX}
  \fontspec{Latin Modern Roman}
  % \setmonofont[Scale=0.85]{Luxi Mono Regular} % muss aktiviert werden,
                                % falls das Paket installiert ist

\else
  %---- Eingabezeichensatz ---------------------------------------------------
  \usepackage[utf8]{inputenc}   % Eingabe deutscher Umlaute
                                % Unix/Linux: utf8
                                % Unix/Linux: latin1 (alt)
                                % Windows: cp1250

  %---- Grundfont ------------------------------------------------------------
  \usepackage[T1]{fontenc}      % ec-Fonts
  \usepackage{lmodern}          % wg. der lm-Fonts (keine bitmap-Fonts!)
\fi

%---- Sprachauswahl ----------------------------------------------------------
\usepackage{babel}              % fuer gemischtsprachigen Einsatz

%---- Verwaltung der Bibliographie, muss nach babel geladen werden -----------
                                % Verwaltung der
                                % Bibliographie durch
\usepackage[backend=biber,      % Biber und biblatex
            autolang=other,     % Trennung gemäß der mit
                                % babel gesetzten Sprache
            style=alphabetic,   % Verweise ähnlich zu
                                % alpha.bst: XXX00
            citestyle=alphabetic, % mehrere Titel eines
                                % Autors werden XXX00a,
                                % XXX00b, ... zitiert
            giveninits=false,   % Vornamen werden nicht
                                % abgekürzt
            ]{biblatex}
\usepackage[babel,german=quotes]{csquotes} % Titel werden
                                % in deutsche Gänsefüßchen
                                % gesetzt
% \addbibresource{...}            % muss mit .bib-Datenbanken gefüllt
                                  % werden
% \ifluatex\else
%   \usepackage{babelbib}       % fuer eine dazu passende Bibliographie,
                                % luatex kennt seine eigene
                                % Bibliographieverwaltung
% \fi

%---- Sonstiges --------------------------------------------------------------
% \PassOptionsToPackage{debugshow,final}{graphicx} % bei Bedarf zu aktivieren
\usepackage{graphicx}           % Vorbereitung der Graphiken
% \graphicspath{{...}{...}}       % muss mit entsprechenden Pfaden
                                % gefüllt werden, die Pfadangabe muss relativ
                                % zum Hautptdokument (\documentclass)
                                % erfolgen und mit einem "/" enden!
\usepackage{listings}
%\usepackage{lstdoc}             % loads the listings package
\usepackage{xcolor}
%---- Bezuege ----------------------------------------------------------------
% gemäß der cleveref Dokumentation müssen diese Pakete als letzte geladen
% werden
\usepackage{varioref}           % Voraussetzung für cleveref
\usepackage{hyperref}           % muss gemäß der Dokumentation Abschnitt
                                % 14.1 an dieser Stelle geladen werden
\usepackage{cleveref}           % Bezuege in der primären Sprache, nach
                                % babel zu laden

%---- Einstellungen ----------------------------------------------------------



%---- Eigene Definitionen ----------------------------------------------------
\title{Testing the \textsf{listings} Language Definition for Rexx}
\author{Prof. Dr. Jobst Hoffmann}

\NewDocumentCommand{\FontLock}{m m}{\makebox[6cm][l]{#1:\hspace{\fill}#2}
  \colorbox{#2}{\phantom{MMMMM}}}

\NewDocumentCommand{\meta}{m}
{%
  {\ensuremath{\langle}\textit{#1}\ensuremath{\rangle}}%
}

\lstset{language=rexx}
\input{listings-rexx.prf}

\begin{document}

\maketitle{}

\tableofcontents{}

\lstlistoflistings{}

\begin{abstract}
    This article shows the support of the \textsf{listings} package for
    Rexx with the flavours Classic Rexx, Rexx VM/Xa and ooRexx 5.10.
\end{abstract}


\section{Preferences for typesetting Rexx scripts: \texttt{listings-rexx.prf}}
\label{sec:pref-types-rexx}

If you want to document a program, the usual way of marking up the program
is done by using different font faces for printing black on white. This is
a very limited approach approach, as there are only a few font faces that
are easy to distinguish visually.

If colors can be used we have a lot more of possibilities to distinguish
the several syntactical elements of modern programming languages. The rule
must be: same syntactical element, same color. So it is a good idea, to
define a common set of colors---the preferences---for highlighting
programs, here for programs using Rexx as programming language.

The essential elements and their correspondent colors are:
\begin{enumerate}
    \lstset{style=rexx-colored}
  \item \FontLock{Keyword}{blue}

    This color is used for the Rexx
    keywords like \lstinline|procedure|, \lstinline|do|, or
    \lstinline|call|.

    In the case of OOP with ooRexx the keywords like \lstinline|::class| or
    \lstinline|::methode| get colorized in the same way.
  \item \FontLock{Comment}{orange}

    Rexx comments can be nested, the color isn't changed for that:
\begin{lstlisting}
return width*height/2 /* this holds for
       /* isosceles triangles */
          any triangle */
\end{lstlisting}
  \item \FontLock{String}{teal}

    Strings can be delimited by either single or double apostrophes:
\begin{lstlisting}
say "Hello, world!"
say 'Hello, world!'
\end{lstlisting}
  \item \FontLock{Builtins}{cyan}

    The builtins are the set of functions from bit opreations over
    converting bases to string operations.
\begin{lstlisting}
say BITAND("13"x,"5555"x,"74"x) /* should give "aa54"x */
say D2X(129)                    /* should give "81"    */
say SUBSTR("abc", 2, 4)         /* should give "bc  "  */
\end{lstlisting}
  \item \FontLock{Special variables}{olive}

    Rexx knows some special variables like \lstinline|rc| or
    \lstinline|result| for the communication between procedures.
  \item \FontLock{Background}{yellow!10}

    The background is already used in the examples above.
\end{enumerate}
which end in the following code (see \vref{lst:rexx-prf}):
\lstinputlisting[language={[LaTeX]tex}, caption={The preferences for the
    markup of Rexx programs}, label=lst:rexx-prf, inputpath=../../../,
  style=rexx-colored]{listings-rexx.prf}


\section{Classic Rexx}
\label{sec:classic-rexx}


The author of this paper doesn't know exactly the differences and assumes
that most of the stuff concerning Rexx in is correct. So here is the
example using Classic Rexx.

Here (\vref{lst:cl-rexx-bw}) is the result of using the \textsf{listings}
package using only different font faces for printing black on white.%
\lstinputlisting[caption={A simple Classic Rexx script, black on white},
  label={lst:cl-rexx-bw}, language=rexx, style=bw]{./rexx_std.rexx}

The same script marked down using colors looks like this:
\vref{lst:cl-rexx-col}.%
\lstinputlisting[caption={A simple Classic Rexx script, colorized},
  label={lst:cl-rexx-col}, language=rexx,
  style=rexx-colored]{./rexx_std.rexx}

\section{VM/XA Rexx}
\label{sec:vmxa-rexx}

VM/XA Rexx offers more control keywords (\lstinline|until|,
\lstinline|while|) and some more builtins, which are used in the scripts
(\vref{lst:rexx-vmxa-bw}, \vref{lst:rexx-vmxa-col}) below.

\lstinputlisting[caption={A Rexx script using VM/XA extensions, black on
    white}, label={lst:rexx-vmxa-bw}, language={[vm/xa]rexx},
  style=bw]{./rexx_vmxa.rexx}


\lstinputlisting[caption={A Rexx script using VM/XA extensions, colorized},
  label={lst:rexx-vmxa-col}, language={[vm/xa]rexx},
  style=rexx-colored]{./rexx_vmxa.rexx}


\section{ooRexx}
\label{sec:oorexx}

ooRexx now supports OOP with (most) of its advantages: data encapsulation,
inheritance, \ldots{}. The two scripts show the black and white and the
colized markdown.

\lstinputlisting[caption={A basic ooRexx script, black and white},
  label={lst:oorexx-i-bw}, language=oorexx, style=bw]{./rexx_oorexx.rexx}

\lstinputlisting[caption={A more sophisticated ooRexx script, black and
    white}, label={lst:oorexx-ii-bw}, language=oorexx,
  style=bw]{./greeter.rexx}

\lstinputlisting[caption={A basic ooRexx script, colorized},
  label={lst:oorexx-i-col}, language=oorexx,
  style=rexx-colored]{./rexx_oorexx.rexx}

\lstinputlisting[caption={A more sophisticated ooRexx script, colorized},
  label={lst:oorexx-ii-col}, language=oorexx,
  style=rexx-colored]{./greeter.rexx}

\end{document}

%%% Local Variables:
%%% LaTeX-fancyvrb-chars: (?|)
%%% TeX-engine: luatex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% mode: latex
%%% End:
