#! /usr/bin/rexx

thisgreeter = .greeter~new("chair polisher")
say thisgreeter
thisgreeter~greet("everybody")


::class greeter public      /* directives */

  ::method init             /* constructor */
    expose identifier
    use arg identifier

  ::method string           /* toString analog */
    expose identifier
    return "Hello, I'm a " || identifier || ","

  ::method greet            /* standard method */
    expose addressee
    use arg addressee
    say "I'm greeting " || addressee || "!"

  ::attribute identifier    /* standard attribute */
