#! /usr/bin/env lua
--- v5_3_test_00.lua ---

-- Author: Prof. Dr. Jobst Hoffmann <j.hoffmann@fh-aachen.de>
-- Version: $Id: v5_3.lua 0 2015/06/03 09:22:14 ax006ho $
-- Keywords:
-- X-URL:

-- a simple Lua v5.3.0 program to show some new keywords

y = 3.005
x = math.tointeger(y)
if x then
   print(x .. "is of type: " .. math.type(x))
else
   print(y .. " can't get converted to integer")
end

y = 3.000
x = math.tointeger(y)
if x then
   print(x .. " is of type: " .. math.type(x))
else
   print(y .. " can't get converted to integer")
end

--- v5_3_test_00.lua ends here
