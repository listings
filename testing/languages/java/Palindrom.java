/**
 * Palindrom.java
 *
 *
 * <br>
 * Created: $Date: 2016/03/03 19:12:57$
 *
 * @author <a href="mailto:j.hoffmann@fh-aachen.de">Prof. Dr. Jobst Hoffmann</a>
 * @version $Revision: 0.0$
 */

public class Palindrom {

    public Palindrom() {

    } // end of default constructor "Palindrom()"

    // be: isPalindrom
    public static boolean isPalindrom(String w) {

        boolean result = false;
        if ( w.length() >= 1 ) {
            result = true;
            for ( int i = 0; i < w.length(); i++ ) {
                if ( !Character.isLetter(w.charAt(i)) ) {
                    result = false;
                }
                char first = Character.toUpperCase(w.charAt(i));
                char sec = Character.toUpperCase(w.charAt(w.length() - i - 1));
                if ( first != sec ) {
                    result = false;
                }
            }
        }
        return result;
    }
// ee: isPalindrom


    public static final void main(final String[] args) {

        String word = null;
        String veryLong = "a";
        String tmp = null;

        word = "Rentner";
        System.out.println(word + " ist " + (isPalindrom(word)? "": "k") + "ein Palindrom");

        word = "Mitarbeiter";
        System.out.println(word + " ist " + (isPalindrom(word)? "": "k") + "ein Palindrom");
        word = "Otto";
        System.out.println(word + " ist " + (isPalindrom(word)? "": "k") + "ein Palindrom");

        // while ( Integer.MAX_VALUE - veryLong.length() > veryLong.length()  ) {
        //     tmp = veryLong + "a";
        //     veryLong = tmp;
        //     System.out.println("curr length: " + veryLong.length());
        // } // end of while (Integer.MAX_VALUE - veryLong.length() > veryLong.length() )

        System.out.println(Integer.MAX_VALUE);
        for ( int i = 0; i <= 30; i++ ) {
            veryLong = veryLong + veryLong;
            System.out.println(i + " - curr length: " + veryLong.length());
        } // end of for (iint i = 0; i <= 10)

        System.out.println("finished");

        tmp = veryLong + veryLong;

    } // end of method "main(String[] args)"

} // end of class "Palindrom"
