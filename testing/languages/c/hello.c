#include <stdio.h>

int main(int iArgC, char **ppszArg)
{

#ifndef ARG
    printf("Hello, world\n");
#else /* ARG */
    printf("Hello, %s\n", ppszArg[1]);
#endif /* ARG */

    return 0;
}
