betrag = InputBox("Geben Sie den Betrag ein!") ' Betrag erfragen

' Formel: netto * mwst = brutto; netto = brutto / mwst
mwst = 1.16  ' 16% MwSt.
netto = betrag / mwst

' Ergebnis als String zusammenstellen:
ergebnis = "netto:" & vbTab & vbTab & FormatCurrency(netto) & vbNewLine
ergebnis = ergebnis & "+16% MwSt:" & vbTab & FormatCurrency(betrag - netto) & vbNewLine
ergebnis = ergebnis & "Gesamt:" & vbTab & vbTab & FormatCurrency(betrag)

' Ergebnis ausgeben
MsgBox ergebnis
