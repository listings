#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
  if (argc > 1) {
    char *lastchar;
    double num = strtod(argv[1], &lastchar);
    if (*lastchar == '\0')
      printf("%f cubed is %f\n", num, num*num*num);
    else
      printf("%s: %s is not a valid number\n", argv[0], argv[1]);
  }
  else
    printf("Usage: %s <number>\n", argv[0]);
  return 0;
}
