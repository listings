; Function Attrs: nounwind uwtable
define double @square(double %x) #0 {
   %1 = alloca double, align 8
   store double %x, double* %1, align 8
   %2 = load double* %1, align 8
   %3 = load double* %1, align 8
   %4 = fmul double %2, %3
   ret double %4
}
