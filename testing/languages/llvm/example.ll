; ModuleID = 'example.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [16 x i8] c"%f cubed is %f\0A\00", align 1
@.str1 = private unnamed_addr constant [30 x i8] c"%s: %s is not a valid number\0A\00", align 1
@.str2 = private unnamed_addr constant [20 x i8] c"Usage: %s <number>\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main(i32 %argc, i8** %argv) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %lastchar = alloca i8*, align 8
  %num = alloca double, align 8
  store i32 0, i32* %1
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  %4 = load i32* %2, align 4
  %5 = icmp sgt i32 %4, 1
  br i1 %5, label %6, label %32

; <label>:6                                       ; preds = %0
  %7 = load i8*** %3, align 8
  %8 = getelementptr inbounds i8** %7, i64 1
  %9 = load i8** %8, align 8
  %10 = call double @strtod(i8* %9, i8** %lastchar) #3
  store double %10, double* %num, align 8
  %11 = load i8** %lastchar, align 8
  %12 = load i8* %11, align 1
  %13 = sext i8 %12 to i32
  %14 = icmp eq i32 %13, 0
  br i1 %14, label %15, label %23

; <label>:15                                      ; preds = %6
  %16 = load double* %num, align 8
  %17 = load double* %num, align 8
  %18 = load double* %num, align 8
  %19 = fmul double %17, %18
  %20 = load double* %num, align 8
  %21 = fmul double %19, %20
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str, i32 0, i32 0), double %16, double %21)
  br label %31

; <label>:23                                      ; preds = %6
  %24 = load i8*** %3, align 8
  %25 = getelementptr inbounds i8** %24, i64 0
  %26 = load i8** %25, align 8
  %27 = load i8*** %3, align 8
  %28 = getelementptr inbounds i8** %27, i64 1
  %29 = load i8** %28, align 8
  %30 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i8* %26, i8* %29)
  br label %31

; <label>:31                                      ; preds = %23, %15
  br label %37

; <label>:32                                      ; preds = %0
  %33 = load i8*** %3, align 8
  %34 = getelementptr inbounds i8** %33, i64 0
  %35 = load i8** %34, align 8
  %36 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([20 x i8]* @.str2, i32 0, i32 0), i8* %35)
  br label %37

; <label>:37                                      ; preds = %32, %31
  ret i32 0
}

; Function Attrs: nounwind
declare double @strtod(i8*, i8**) #1

declare i32 @printf(i8*, ...) #2

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
