#! /usr/bin/env python

import re
def findpattern(pattern, lines):
    """
    Return the lines that match regexp `pattern`
    """
    matches = [ ]
    for line in lines:
        if re.search(pattern, line):
            matches.append(line)
    return matches

emma = open('austen-emma.txt')
emmalines = emma.readlines()
results = findpattern(r"\bacquit\b", emmalines)
print (results[1])
