! Time-stamp: <2024-02-17 11:40:21 ax006ho>
!
! a senseless Fortran 90 program
!
program senseless

  implicit none

  ! declarations:
  integer :: i
  character(len=*), parameter :: o_format = &
       "('i = ', i0, ', i^2 = ', i2, ', f = ', f5.2)"
  character(len=2), dimension(3) :: msg
  real, dimension(3) :: v = (/ (i-2, i = 1, 3) /)

  do i = 1, 5
     write (unit=*, fmt=o_format) i, i*i, f(real(i))
     call s
  end do

  write (*, "(/3e15.7)") v
  where ( v /= 0 )
     msg = "ok"
  elsewhere
     msg = "na"
  endwhere
  write (*, fmt="(3a15)") msg

  stop

contains

  function f(x) result(y)

    real :: x
    real :: y

    y = 2*x +2

  end function f

  subroutine s()

    integer, save :: i = 0

    i = i + 1

    write (*, *) "call no. ", i

  end subroutine s

end program senseless
