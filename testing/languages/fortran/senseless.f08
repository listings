! $Id$
!
! a senseless Fortran 2008 program
!
program senseless

  implicit none
  integer :: i

  do i  = 1, 2
     write (unit=*, fmt=*) "outer:", i
     block
         integer :: j
         integer :: i = 2  ! hide i,
                           ! implicit save
         i = i + 2
         do j = 1, 2
            write (unit=*, fmt=*) "inner:", i, sin(real(j))
         end do
     end block
     write (unit=*, fmt=*) trailz(i), bge(i, 0)
  end do
end program senseless
