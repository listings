import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  * Context Functions:
  *  - https://dotty.epfl.ch/docs/reference/contextual/context-functions.html
  *  - https://www.scala-lang.org/blog/2016/12/07/implicit-function-types.html
  */
object ContextFunctions:
  sealed trait X:
    def x: Double
    def tpe: X
  sealed trait Y:
    def y: Double
    def tpe: Y
  export X.{ _, given }
  export Y.{ *, given }
  type P = Y & X
  type PP = X & Y
  type M = [X, Y] =>> Map[Y, X]
  type Tuple = [X] =>> (X, X)
  enum ListEnum[+A]: 
    case Cons(h: A, t: ListEnum[A])
    case Empty
  enum Planet(mass: Double, radius: Double):
    private final val G = 6.67300E-11
    def surfaceGravity = G * mass / (radius * radius)
    def surfaceWeight(otherMass: Double) =  otherMass * surfaceGravity
    case Mercury extends Planet(3.303e+23, 2.4397e6)
    case Venus   extends Planet(4.869e+24, 6.0518e6)
    case Earth   extends Planet(5.976e+24, 6.37814e6)
  given IntWrapperToDoubleWrapper: Conversion[IntWrapper, DoubleWrapper] = new Conversion[IntWrapper, DoubleWrapper] {
    override def apply(i: IntWrapper): DoubleWrapper = DoubleWrapper(i.a.toDouble)
  }
  given optionParser[A](using parser: => StringParser[A]): StringParser[Option[A]] = new StringParser[Option[A]] {
    override def parse(s: String): Try[Option[A]] = s match
    case "" => Success(None) // implicit parser not used.
    case str => parser.parse(str).map(x => Some(x)) // implicit parser is evaluated at here
  }
  open case class A(x: int)
  private def printMessages(msgs: (A | B)*) = println(msgs.map(_.msg).mkString(" "))
  object context:
    // type alias Contextual
    type Contextual[T] = ExecutionContext ?=> T
    // sum is expanded to sum(x, y)(ctx)
    def asyncSum(x: Int, y: Int): Contextual[Future[Int]] = Future(x + y)
    def asyncMult(x: Int, y: Int)(using ctx: ExecutionContext) = Future(x * y)

  object parse:
    def unapplySeq(name: String): Option[Seq[String]] =
      val names = name.trim.split(" ")
      if names.size < 2 then None
      else Some(names.last :: names.head :: names.drop(1).dropRight(1).toList)
      "even" match
        case s @ Even() => println(s"$s has an even number of characters")
        case s          => println(s"$s has an odd number of characters")
      val divisionResultFailure: Success | DivisionByZero = safeDivide(4, 0)
    type Parseable[T] = GivenInstances.StringParser[T] ?=> Try[T]
    def sumStrings(x: String, y: String): Parseable[?] =
      val parser = summon[GivenInstances.StringParser[Int]]
      val tryA = parser.parse(x)
      val tryB = parser.parse(y)
      println(List(1, 2) == Vector(1, 2))
      xs.zipWithIndex.map((s, i) => println(s"$i: $s"))

      for
        a <- tryA
        b <- tryB
      yield a + b
