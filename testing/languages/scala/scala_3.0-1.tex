%%% scala_3.0-1.tex ---
%
%% Author: j.hoffmann@fh-aachen.de
%% Time-stamp: <2024-03-11 22:55:49 ax006ho>
%% Keywords:
%% X-URL:

\documentclass[12pt,a4paper,english]{scrartcl}
                                % bei gemischtsprachigem Satz ist die
                                % zuletzt geladene Sprache aktiv
\IfClassLoadedTF{article}{%
  \usepackage{typearea}         % ein auf DIN A4 abgestimmtes Seitenformat,
}{}                             % wird nur bei article gebraucht

\usepackage{iftex}
\ifluatex                       % wenn mit dem neuen TeX-Prozessor
                                % gearbeitet wird
  %---- Eingabezeichensatz ---------------------------------------------------
                                % luatex unterstützt utf-8, also keine
                                % Festlegung des Eingabezeichensatzes
                                % erforderlich
  %---- Grundfont ------------------------------------------------------------
  \usepackage{fontspec}         % Festlegen der Fontverwaltung für LuaTeX.
  \defaultfontfeatures{Ligatures=TeX}
  \fontspec{Latin Modern Roman}
  % \setmonofont[Scale=0.85]{Luxi Mono Regular} % muss aktiviert werden,
                                % falls das Paket installiert ist

\else
  %---- Eingabezeichensatz ---------------------------------------------------
  \usepackage[utf8]{inputenc}   % Eingabe deutscher Umlaute
                                % Unix/Linux: utf8
                                % Unix/Linux: latin1 (alt)
                                % Windows: cp1250

  %---- Grundfont ------------------------------------------------------------
  \usepackage[T1]{fontenc}      % ec-Fonts
  \usepackage{lmodern}          % wg. der lm-Fonts (keine bitmap-Fonts!)
\fi

%---- Sprachauswahl ----------------------------------------------------------
\usepackage{babel}              % fuer gemischtsprachigen Einsatz

%---- Verwaltung der Bibliographie, muss nach babel geladen werden -----------
                                % Verwaltung der
                                % Bibliographie durch
\usepackage[backend=biber,      % Biber und biblatex
            autolang=other,     % Trennung gemäß der mit
                                % babel gesetzten Sprache
            style=alphabetic,   % Verweise ähnlich zu
                                % alpha.bst: XXX00
            citestyle=alphabetic, % mehrere Titel eines
                                % Autors werden XXX00a,
                                % XXX00b, ... zitiert
            giveninits=false,   % Vornamen werden nicht
                                % abgekürzt
            ]{biblatex}
\usepackage[babel,german=quotes]{csquotes} % Titel werden
                                % in deutsche Gänsefüßchen
                                % gesetzt
% \addbibresource{...}            % muss mit .bib-Datenbanken gefüllt
                                  % werden
% \ifluatex\else
%   \usepackage{babelbib}       % fuer eine dazu passende Bibliographie,
                                % luatex kennt seine eigene
                                % Bibliographieverwaltung
% \fi

%---- Sonstiges --------------------------------------------------------------
% \PassOptionsToPackage{debugshow,final}{graphicx} % bei Bedarf zu aktivieren
\usepackage{graphicx}           % Vorbereitung der Graphiken
% \graphicspath{{...}{...}}       % muss mit entsprechenden Pfaden
                                % gefüllt werden, die Pfadangabe muss relativ
                                % zum Hautptdokument (\documentclass)
                                % erfolgen und mit einem "/" enden!
\usepackage{listings}
\usepackage{xcolor}
%---- Bezuege ----------------------------------------------------------------
% gemäß der cleveref Dokumentation müssen diese Pakete als letzte geladen
% werden
\usepackage{varioref}           % Voraussetzung für cleveref
\usepackage{hyperref}           % muss gemäß der Dokumentation Abschnitt
                                % 14.1 an dieser Stelle geladen werden
\usepackage{cleveref}           % Bezuege in der primären Sprache, nach
                                % babel zu laden

%---- Einstellungen ----------------------------------------------------------



%---- Eigene Definitionen ----------------------------------------------------
\title{Testing Scala 3.0}
\author{Prof. Dr. Jobst Hoffmann}

\NewDocumentCommand{\FontLock}{m m}{\makebox[6cm][l]{#1:\hspace{\fill}#2}
  \colorbox[HTML]{#2}{\phantom{MMMMM}}}

\begin{document}

\maketitle{}

\tableofcontents{}

\lstlistoflistings{}

\begin{abstract}
    This article shows the listings's support for Scala 3.0.
\end{abstract}


\section{The changes from Scala 2.0 to 3.0}
\label{sec:changes-from-scala}

There are some changes in the language definition:
\begin{enumerate}
  \item new keywords,
  \item deleted keywords, and
  \item special keywords.

    \lstMakeShortInline|%
    The latter are known as soft modifiers---the identifiers |infix|,
    |inline|, |opaque|, |open| and |transparent|---and soft keywords, which are
    a super set of the soft modifiers---|as|, |derives|, |end|,
    |extension|, |throws|, |using|, \lstinline-|-, |+|, |-|, |*| are added to the
    above mentioned soft modifiers.
    \lstDeleteShortInline|%

    These special keywords can't be handled by the \textsf{listings}
    package, because they are treated specially in some situations
    (\url{https://docs.scala-lang.org/scala3/reference/soft-modifier.html}),
    otherwise they are treated as normal identifiers.
\end{enumerate}

The \vref{lst:test-hightlight} uses this code, using the settings shown in
\vref{lst:lst-cfg}, which are used by default by the author of these pages.%
\lstinputlisting[caption={Using 3.0 language elements, language Scala 3.0
    selected, default values}, label=lst:test-hightlight,
  language={[3.0]scala}]{./TestHighlight.scala}


\section{Working with Scala 3.0}
\label{sec:simple-scala-3.0}

Scala programs can be developed with any editor, but an IDE can simplify
developing. An example for such an IDE is VS Code from Microsoft.


\subsection{Developing Scala 3.0 programs supported by VS Code}
\label{sec:developing-scala-3.0}

VS Code colorizes the code presented in \vref{lst:test-hightlight} like
shown in \vref{fig:excerpt-from-scala-vscode}, the meaning of the colors is
described below:
\begin{enumerate}
  \item \FontLock{Keyword}{569cd6}
  \item \FontLock{Comment}{6a9955}
  \item \FontLock{String}{ce9178}
  \item \FontLock{Control keyword}{c586c0}
  \item \FontLock{Format descriptor(?)}{559ad3}
  \item \FontLock{Link}{1b6acb}
  \item \FontLock{Parameter name, val}{9cdcfe}
  \item \FontLock{Type, package name}{4ec9b0}
  \item \FontLock{Variable name}{dcdcaa}
  \item \FontLock{\lstinline|case|}{c183bc}
  \item \FontLock{\lstinline|enum|}{5599d1}
\end{enumerate}
The first three can be easily configured for the use with Emacs, the rest
of the list must be supplied by the user, which can be a tedious task.
\begin{figure}[p]
    \centering%
    \includegraphics[width=1.0\textwidth,]{scala-vs_code}
    \caption{Excerpt from a Scala 3.0 program, coloured by VS Code}
    \label{fig:excerpt-from-scala-vscode}
\end{figure}

\input{./listings-scala-vs_code.prf}

\lstinputlisting[caption={Using 3.0 language elements, language Scala 3.0
    selected, colors according VS code}, label={lst:test-hightlight},
  language={scala}, style=vscode]{./TestHighlight.scala}


\subsection{Developing Scala 3.0 programs supported by Emacs}
\label{sec:developing-scala-3.0-1}

Emacs colorizes the above mentioned code with colors as shown in
\vref{fig:excerpt-from-scala-emacs}, their meaning is shown below:
\begin{itemize}
  \item \FontLock{Keyword}{800080}
  \item \FontLock{Block comment, string}{8b2252} % /* ... */ or "even"
  \item \FontLock{End line comment}{b22222}
  \item \FontLock{Attribute}{483d8b} % sealed
  \item \FontLock{Package name, object}{008b8b} % e.g. Try or NextFunction
  \item \FontLock{Type}{228b22} % e.g. Double
  \item \FontLock{Variable name}{0000ff}
  \item \FontLock{\lstinline|val|}{a0522d}
\end{itemize}
As before only the first three colors can be automatically distinguished by
the \textsf{listings} package
\begin{figure}[p]
    \centering%
    \includegraphics[width=1.0\textwidth]{scala-emacs}
    \caption{Excerpt from a Scala 3.0 program, coloured by Emacs}
    \label{fig:excerpt-from-scala-emacs}
\end{figure}

\input{./listings-scala-emacs.prf}

\lstinputlisting[caption={Using 3.0 language elements, language Scala 3.0
    selected, colors according Emacs}, label=lst:test-highlight,
  language={[3.0]scala}, style=emacs]{./TestHighlight.scala}
\end{document}

%%% Local Variables:
%%% LaTeX-fancyvrb-chars: (?|)
%%% TeX-engine: luatex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% mode: latex
%%% End:
