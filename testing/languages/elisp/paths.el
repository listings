;:*===================================================================
;:*                 START (X)EMACS PATH SETTINGS
;:*===================================================================

;:*===================================================================
;:*             Customization of Personal Path Settings
;:*
;:* for ease of loading and compiling set cursor on point after ')'
;:* and enter C-x C-e
;:*     (load-file "~/etc/XEmacs/paths-jhf.el")
;:*     (byte-compile-file "~/etc/XEmacs/paths-jhf.el")
;:*
;:* excerpts from sample.emacs and
;:*          from keys.el (R. Socha)

;:*======================
;:* define the search path for the common packages

(if show-load-path
    (message "\nload path before: %s\n" load-path))

(if (string-match "XEmacs" XEmacs-name-JHf)
    (if (and (boundp 'beta-version) beta-version)
        (progn
          (message "this is beta: %s" beta-version)
          (setq
           global-package-path "/usr/local/share/xemacs/xemacs-packages/lisp/"
           local-package-path "~/etc/XEmacs/xemacs-packages/lisp/"))
      (progn
        (message "%s" "this is a released version")
        (setq
         global-package-path "/opt/local/lib/xemacs/xemacs-packages/lisp/"
         local-package-path "~/etc/XEmacs/xemacs-packages/lisp/")))
  (progn
    (if show-load-path
        (message "\n\nsetting path for emacs\n\n"))
    (setq
     ;; now using standard paths, so these settings aren't needed anymore
     ;; global-package-path (concat
     ;;                      "/usr/local/share/emacs/site-lisp/ "
     ;;                      "/usr/share/emacs/site-lisp/")
     ;; local-package-path "~/etc/emacs/site-lisp/"
     global-package-path ""
     local-package-path "")))


;(add-to-list 'load-path
;             local-package-path)
;(add-to-list 'load-path
;             global-package-path)


;:*======================
;:* add some directories with special components
(if (string-match "XEmacs" XEmacs-name-JHf)
    (progn
      (add-to-list 'load-path           ; .el-files for yasnippet stay here
                   "~/etc/XEmacs/xemacs-packages/lisp/yasnippet")
    (add-to-list 'load-path             ; .el-files for gnuplot stay here
                 "/opt/gnu/share/emacs/site-lisp/")
    (add-to-list 'load-path             ; .el-files for org-mode stay here
                 "/usr/local/share/xemacs/xemacs-packages/lisp/org"))
  (progn                                ; load path for emacs
    ;; the next two entries are standards, so they may be removed in the
    ;; near future
    ;; (add-to-list 'load-path
    ;;      "/usr/local/share/emacs/site-lisp/psgml")
    ;; (add-to-list 'load-path
    ;;      "~/etc/emacs/site-lisp/yasnippet")
    (add-to-list 'load-path
                      "~/.emacs.d/plugins/yasnippet")
    (add-to-list 'load-path
                      "~/.emacs.d/elpa/pdf-tools")))

;:*======================
;:* some packages need special care, as they stay in their own
;:* directories
;(if (and (boundp 'beta-version) beta-version)
;    (progn
;      (message "%s" "no special handling for jde beta"))
;    (progn
;      (message "%s" "loading from the original distribution")
;(add-to-list 'load-path
;             (expand-file-name "jde/lisp" global-package-path))))
(add-to-list 'load-path
             (expand-file-name "octave" global-package-path))
; (add-to-list 'load-path
;              (expand-file-name "etc/emacs/25.2/lisp-src/groovy-emacs-modes" global-package-path))

(if show-load-path
    (message "\nload path after: %s\n" load-path))

;:*===================================================================
;:* End of customized settings

(provide 'paths-jhf)
;:*===================================================================
;:*  Controlling editing this file
;:*===================================================================
;: Local Variables:
;: mode: Emacs-Lisp
;: eval: (outline-minor-mode)
;: outline-regexp: ";:\\*\\**"
;: End:

;:*===================================================================
;:* COPYRIGHT
;:* Copyright 2004-2018 Dr. Jobst Hoffmann
;:* Author: j.hoffmann@fh-aachen.de ($Author: ax006ho $)
;:* Version: $Id: paths-jhf.el 287 2012-11-05 18:30:18Z ax006ho $
;:* Keywords:
;:* X-URL: not distributed yet...
;:*
;:*===================================================================
;:* INSTRUCTIONS
;:* This file starts in outline-minor-mode.
;:* To edit this file, use the menu entries Show ... to show a topic
;:* and Hide ... to hide it again.
;:*
;:* The keybindings for Emacs 26.1 are
;:* C-c @ C-t (hiding) and
;:* C-c @ C-a (show all)
;:*
;:*===================================================================
;:* This code is free software; you can redistribute it and/or modify
;:* it under the terms of the GNU General Public License as published by
;:* the Free Software Foundation; either version 2 of the License, or
;:* (at your option) any later version.
;:*
;:* This code is distributed in the hope that it will be useful,
;:* but WITHOUT ANY WARRANTY; without even the implied warranty of
;:* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;:* GNU General Public License for more details.
;:*
;:* You should have received a copy of the GNU General Public License
;:* along with Emacs (which is required to make this stuff work); if
;:* not, write to the Free Software Foundation, Inc., 675 Mass Ave,
;:* Cambridge, MA 02139, USA.
;:*
;:* paths-jhf.el ends here
