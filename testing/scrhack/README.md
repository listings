# scrhack - try to make scrhack unnecessary #

At the moment using the listings package in combination with any of the
koma package gives a log entry like

    Class scrreprt Warning: \float@listhead detected!
    (scrreprt)              Implementation of \float@listhead became
    (scrreprt)              deprecated in KOMA-Script v3.01 2008/11/14 and
    (scrreprt)              has been replaced by several more flexible
    (scrreprt)              features of package `tocbasic`.
    (scrreprt)              Maybe implementation of \float@listhead will
    (scrreprt)              be removed from KOMA-Script soon.
    (scrreprt)              Loading of package `scrhack' may help to
    (scrreprt)              avoid this warning, if you are using a
    (scrreprt)              a package that still implements the
    (scrreprt)              deprecated \float@listhead interface on input line 92.

This text is logged independently of the context (caption or no caption,
\lstlistoflistings or not.

The standard packages do not test this.
