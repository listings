#! /bin/sh
### hello.sh ---

## Author: Prof. Dr. Jobst Hoffmann <j.hoffmann@fh-aachen.de>
## Version: $Id: hello.sh 0 <2023/03/04 09:18:55> ax006ho Exp $
##          update date and time by C-u M-x org-time-stamp
## Keywords:
## X-URL:

#=----------------------------------------------------------------------
#=- error handling according to
#=- https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail
#=- http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -Eeuo pipefail

trap "echo ERR trap fired!" ERR

#=----------------------------------------------------------------------
#=- global functions
#=-

##=---------------------------------------------------------------------
##=- <function name>:
##=- parameter/s:
##=-
##=- [<description>]

##=---------------------------------------------------------------------


#=----------------------------------------------------------------------
#=- start of executable code
main() {
    local rc=0

    printf "%s\n" "Hello, world!"

    return ${rc}
} # end of function main


{
    ##=-----------------------------------------------------------------
    ##=- global variables

    ###=- load the BMS ...
    if ! . bms.sh; then
        exit 1
    fi
    ###=- ... and some modules
    if ! include "ctrl"; then   # "ctrl" provides several exit codes and
        exit 1                  # "log"
    fi

    main "${@}"
    exit "${?}"
} # end of executable code


# Local Variables:
# mode: sh
# outline-regexp: "^ *#+=-.*"
# End:

### hello.sh ends here
