%%% listings-lua.tex
%%% Copyright 2013 Stephan Hennig
%%
%% This work may be distributed and/or modified under the conditions of
%% the LaTeX Project Public License, either version 1.3 of this license
%% or (at your option) any later version.  The latest version of this
%% license is in http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% See file README for more information.
%%
\documentclass[11pt]{article}
\usepackage[ansinew]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}
\usepackage{multicol}
\usepackage[rgb, x11names]{xcolor}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  columns=spaceflexible,
}
% Short-cut for non-language code snippets.
\lstMakeShortInline\|
% Short-cut for LaTeX code snippets.
\lstMakeShortInline[
language={[LaTeX]TeX},
basicstyle=\ttfamily,
]�
\lstdefinestyle{LaTeX}{%
  language={[LaTeX]TeX}
}
\usepackage{xspace}
\usepackage{array}
\usepackage{booktabs}
\usepackage[latin, UKenglish]{babel}
\usepackage{hyperref}
\hypersetup{
  pdftitle={listings-lua},
  pdfauthor={Stephan Hennig},
  pdfkeywords={listings, Lua, syntax highlighting, TeX, LaTeX}
}
\hypersetup{
  english,% For \autoref.
  pdfstartview={XYZ null null null},% Zoom factor is determined by viewer.
  colorlinks,
  linkcolor=RoyalBlue3,
  urlcolor=Chocolate4,
  citecolor=DeepPink2
}
\newcommand*{\pkg}{\textsf{listings-lua}}
\newcommand*{\descr}[1]{$\langle$\emph{#1}$\rangle$}
\newcommand*{\latinphrase}[1]{\foreignlanguage{latin}{\emph{#1}}}
\begin{document}
\author{Stephan Hennig\thanks{sh2d@arcor.de}}
\title{\pkg\thanks{This document describes the \pkg\ package v0.3.}}
\maketitle


\begin{abstract}
  This package adds support for the Lua language to the |listings|
  package.  Lua versions~5.0, 5.1 and 5.2 are supported.
\end{abstract}

\begin{multicols}{2}
\small
% Set toc entries ragged right.  Trick taken from tocloft.pdf.
\makeatletter
\renewcommand{\@tocrmarg}{2.55em plus1fil}
\makeatother
\tableofcontents
\end{multicols}


\section{Introduction}
\label{sec:intro}

The |listings| package is badly missing support for the Lua language.
This package adds language definitions for Lua versions~5.0, 5.1 and
5.2.  Highlighting of the following language features is supported:

\begin{itemize}
\item language keywords,
\item standard library identifiers,
\item labels,
\item strings delimited by single and double quotes,
\item single line comments,
\item long bracket strings and comments up to level 10.
\end{itemize}


\section{Usage}
\label{sec:usage}

To load the package, add the line

\begin{lstlisting}[style=LaTeX]
\usepackage{listings-lua}
\end{lstlisting}
%
in the preamble of your \LaTeX document.  To activate the Lua language
in a listing, the |language| key has to be set-up with a value of the
form \texttt{[\descr{dialect}]Lua}, where \texttt{\descr{dialect}}
corresponds to the Lua version requested and is one of �5.0�, �5.1� and
�5.2�.  The package doesn't define a default dialect.  A dialect has
\emph{always} to be passed to the |language| key, \emph{e.\,g.}

\begin{lstlisting}[style=LaTeX]
\lstinputlisting[language={[5.2]Lua}]{myfile.lua}
\end{lstlisting}
%
An additional pair of braces has been used in this example, because
we're already within square brackets.

Keyword class offsets used for defining different language features are
shown in~\autoref{tab:classoffsets}.  \autoref{lst:style-lua} shows an
exemplary style definition for typesetting source code in the Lua~5.2
language.  Different language features are highlighted using
\textsc{rgb} colors from the \textsc{x11} name scheme provided by the
|xcolor| package.

\begin{table}
  \centering
  \begin{tabular}{ll}
    class offset & language feature\\
    \addlinespace
    \toprule
    \addlinespace

    1 & language keywords\\
    2 & standard library identifiers and internal variables\\
    3 & labels\\
  \end{tabular}
  \caption{Keyword class offsets used for different language features.}
  \label{tab:classoffsets}
\end{table}

\begin{lstlisting}[style=LaTeX, float, label=lst:style-lua, caption={Highlighting different language features by colour.}]
\usepackage[rgb, x11names]{xcolor}
\lstdefinestyle{Lua}{
  language=[5.2]Lua,
  basicstyle=\ttfamily,
  columns=spaceflexible,
  keywordstyle=\bfseries\color{Blue4},% language keywords
  keywordstyle=[2]\bfseries\color{RoyalBlue3},% std. library identifiers
  keywordstyle=[3]\bfseries\color{Purple3},% labels
  stringstyle=\bfseries\color{Coral4},% strings
  commentstyle=\itshape\color{Green4},% comments
}
\end{lstlisting}

After putting the lines from \autoref{lst:style-lua} into the preamble
of a document, the style can be used as follows:

\begin{lstlisting}[style=LaTeX]
\lstinputlisting[style=Lua]{myfile.lua}
\end{lstlisting}
%
Note, the style definition is \emph{not} part of the \pkg\ package.


\section{What doesn't work}
\label{sec:not-working}

\begin{description}

\item[Lua 5.0] Nested long strings and comments are not supported.

\item[Lua 5.1 and 5.2] While there is support for long bracket strings
  |[[...]]|, |[=[...]=]| \emph{etc.} and comments |--[[...]]|,
  |--[=[...]=]| \emph{etc.}, each long bracket level has to be defined
  explicitly.  Currently, long brackets of up to level~10 are supported.

  You can easily add support for long bracket strings or comments of a
  particular level by defining a custom Lua dialect as follows:

\begin{lstlisting}[style=LaTeX]
\lstdefinelanguage[my5.2]{Lua}[5.2]{Lua}{%
  % level 12
  morecomment=[s]{--[============[}{]============]},%
  morestring=[s]{[============[}{]============]},%
}
\end{lstlisting}

\end{description}

Patches welcome!

\bigskip
\emph{Happy \TeX ing!}


\end{document}



%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: t
%%% coding: latin-1
%%% End: 
